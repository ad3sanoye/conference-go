from django.http import JsonResponse

from .models import Conference, Location
from common.json import ModelEncoder


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    fields = ["name"]


class LocationListEncoder(ModelEncoder):
    model = Location
    fields = ["name"]


class ConferenceDetailEnconder(ModelEncoder):
    model = Conference
    fields = ["name", "starts", "ends", "description", "created", "updated", "max_presentations", "max_attendees", "location", ]
    encoders = {"location": LocationListEncoder}


class LocationDetailEncoder(ModelEncoder):
    model = Location
    fields = ["name", "city", "room_count", "created", "updated", "state", ]

    def get_extra_data(self, obj):
        return {"state": obj.state.abbreviation}


def api_list_conferences(request):
    """
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    """
    conferences = Conference.objects.all()
    return JsonResponse(
        {"conferences": conferences},
        encoder=ConferenceListEncoder,
    )


def api_show_conference(request, id):
    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    """
    conference = Conference.objects.get(id=id)
    return JsonResponse(conference, encoder=ConferenceDetailEnconder, safe=False,)


def api_list_locations(request):
    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    """
    locations = Location.objects.all()
    return JsonResponse(
        {"locations": locations},
        encoder=LocationListEncoder
    )


def api_show_location(request, id):
    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    """
    location = Location.objects.get(id=id)
    return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)
