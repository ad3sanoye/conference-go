from django.http import JsonResponse

from .models import Attendee

from common.json import ModelEncoder


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    fields = ["name"]


# class AttendeeDetailEncoder(ModelEncoder):
#     model = Attendee
#     fields = ["email", "name", "company_name", "created", "conference", ]
#     encoders = {"conference": AttendeeListEncoder}


def api_list_attendees(request, conference_id):
    attendees = Attendee.objects.all()
    return JsonResponse(
        {"attendees": attendees},
        encoder=AttendeeListEncoder,
        )


def api_show_attendee(request, id):
    attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        {
            "email": attendee.email,
            "name": attendee.name,
            "company_name": attendee.company_name,
            "created": attendee.created,
            "conference": {
                "name": attendee.conference.name,
                "href": attendee.conference.get_api_url(),
            },
        }
    )
