from django.http import JsonResponse

from .models import Presentation

from django.core.exceptions import ObjectDoesNotExist

from common.json import ModelEncoder


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    fields = ["presenter_name", "company_name", "presenter_email", "title", "synopsis", "created", ]


def api_list_presentations(request, conference_id):
    """
    Lists the presentation titles and the link to the
    presentation for the specified conference id.

    Returns a dictionary with a single key "presentations"
    which is a list of presentation titles and URLS. Each
    entry in the list is a dictionary that contains the
    title of the presentation, the name of its status, and
    the link to the presentation's information.

    """
    presentations = [
        {
            "title": p.title,
            "status": p.status.name,
            "href": p.get_api_url(),
        }
        for p in Presentation.objects.filter(conference=conference_id)
    ]
    return JsonResponse({"presentations": presentations})


def api_show_presentation(request, id):
    try:
        presenation = Presentation.objects.get(id=id)
        return JsonResponse(presenation, encoder=PresentationDetailEncoder, safe=False)
    except ObjectDoesNotExist:
        return JsonResponse(
            {"error": "Presentation does not exist"}, status=404
        )
