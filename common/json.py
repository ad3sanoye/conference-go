from typing import Any
from django.core.serializers.json import DjangoJSONEncoder
from json import JSONEncoder
from django.db.models import QuerySet


class QuerySetEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, QuerySet):
            return list(obj)
        else:
            return super().default(obj)


class ModelEncoder(DjangoJSONEncoder, QuerySetEncoder):
    encoders = {}

    def default(self, obj):
        if isinstance(obj, self.model):
            encoded = {}
            if hasattr(obj, "get_api_url"):
                encoded["href"] = obj.get_api_url()
            for field in self.fields:
                if field in self.encoders:
                    encoder = self.encoders[field]()
                    encoded[field] = encoder.default(getattr(obj, field))
                else:
                    encoded[field] = getattr(obj, field)
            encoded.update(self.get_extra_data(obj))
            return encoded
        return super().default(obj)

    def get_extra_data(self, obj):
        return {}
